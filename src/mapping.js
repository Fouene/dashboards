/* global mapboxgl */

const isValidGeopoint = (gp) => {
  // geopoints should match format '47.4719575 8.307597656249996 -1.0 -1.0'
  const pattern = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)[\s]?[-+]?(\d+\.?\d*|\d*\.?\d+)[\s]?[-+]?(\d+\.?\d*|\d*\.?\d+)$/;
  // const pattern2 = /^(\-?\d+(\.\d+)?)\s*(\-?\d+(\.\d+)?)\s*(\-?\d+(\.\d+)?)\s*(\-?\d+(\.\d+)?)$/;
  return pattern.test(gp);
};

const parseGeopointString = (gp) => {
  const [lat, lon] = gp.split(' ').map(Number);
  if (!lat || !lon) {
    return null;
  }
  return {
    lat,
    lon,
  };
};

const geopointToGeojson = ({ lat, lon }, label = '', title = '', description = '') => ({
  type: 'Feature',
  geometry: {
    type: 'Point',
    coordinates: [lon, lat],
  },
  properties: {
    name: label,
    title,
    description,
  },
});

const createMap = (mapCenter, element) => {
  const target = element.getElementsByClassName('target')[0];
  target.id = 'mapboxContainer';

  window.mapboxgl.accessToken = 'pk.eyJ1Ijoib3Vyc2NpIiwiYSI6ImNqb2ljdHMxYjA1bDAzcW03Zjd0cHBsbXMifQ.rL9QPLvi0kLP3DzLt1PQBA';

  const map = new mapboxgl.Map({
    container: 'mapboxContainer',
    style: 'mapbox://styles/mapbox/satellite-v9',
    center: mapCenter,
    zoom: 9,
  });
  console.log(map);
  return map;
};

const mapStuff = (survey, formDef, element, color) => {
  const geopointsData = formDef.items
    // filter to geopoint question items
    .filter(({ type }) => type === 'geopoint')
    // map to form data
    .map(({ id }) => survey[id])
    // filter to only form data with valid geopoint entries
    .filter(pointsList => pointsList.some(isValidGeopoint));

  console.log('geopoints data');
  console.log(geopointsData);

  if (geopointsData.length > 0) {
    const pins = geopointsData[0].map(parseGeopointString).filter(i => i !== null);
    console.log(pins);

    const geojsonPins = pins.map((x, index) => geopointToGeojson(x, `Location ${index}`));

    const mapCenter = pins
      .reduce(([rlon, rlat], { lon, lat }) => [rlon + lon, rlat + lat], [0, 0])
      .map(x => x / pins.length);

    const map = createMap(mapCenter, element);
    console.log(geojsonPins);
    geojsonPins.forEach((pin) => {
      const pinEl = document.createElement('i');
      pinEl.className = 'fas fa-map-pin';
      pinEl.style = `font-size: 1.5em; color: ${color}`;
      // pinEl.style.width = '10px';
      // pinEl.style.height = '10px';
      // pinEl.style.borderRadius = '50%';
      // pinEl.style.backgroundColor = 'green';

      new mapboxgl.Marker(pinEl)
        // .setLngLat([pin.lat, pin.long])
        .setLngLat(pin.geometry.coordinates)
        // .setPopup(new mapboxgl.Popup({offset: 25}).setHTML(`<div>${pin.properties.name}</div>`))
        .addTo(map);
    });
  }
};

export default mapStuff;
